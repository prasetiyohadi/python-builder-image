# Copyright (c) 2021 Prasetiyo Hadi Purwoko <prasetiyohadi92@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License

ARG CONTAINER_IMAGE=pras/python-base-image:latest
ARG REMOTE_SOURCE=.
ARG REMOTE_SOURCE_DIR=/remote-source
ARG REMOTE_SOURCE_APP_DIR=$REMOTE_SOURCE_DIR

FROM $CONTAINER_IMAGE as builder
# =============================================================================
ARG REMOTE_SOURCE_DIR
COPY $REMOTE_SOURCE $REMOTE_SOURCE_DIR

FROM $CONTAINER_IMAGE
# =============================================================================
ARG REMOTE_SOURCE_APP_DIR

COPY --from=builder $REMOTE_SOURCE_APP_DIR/build-requirements.txt $REMOTE_SOURCE_APP_DIR/build-requirements.txt
COPY --from=builder $REMOTE_SOURCE_APP_DIR/requirements.txt $REMOTE_SOURCE_APP_DIR/requirements.txt
COPY --from=builder $REMOTE_SOURCE_APP_DIR/scripts/assemble /usr/local/bin/assemble
COPY --from=builder $REMOTE_SOURCE_APP_DIR/scripts/get-extras-packages /usr/local/bin/get-extras-packages
COPY --from=builder $REMOTE_SOURCE_APP_DIR/scripts/install-from-bindep /output/install-from-bindep

WORKDIR $REMOTE_SOURCE_APP_DIR

RUN apt-get update && apt-get upgrade -y \
  && apt-get install -y git \
  && apt-get autoremove -y && apt-get clean all \
  && rm -rf /var/lib/apt/lists/* \
  && rm -rf /var/log/*

RUN cat build-requirements.txt requirements.txt | sort > upper-constraints.txt \
  && pip3 install --no-cache-dir -r build-requirements.txt -c upper-constraints.txt \
  && pip3 install --no-cache-dir -r requirements.txt -c upper-constraints.txt

WORKDIR /
RUN rm -rf $REMOTE_SOURCE_APP_DIR
